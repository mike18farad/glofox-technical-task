package glofoxtechnicaltask

import (
	"context"
	"time"
)

// StudioClass is a representation of Studio Class for which studio members can attend
type StudioClass struct {
	ID        int       `json:"id"`
	Name      string    `json:"name"`
	StartDate time.Time `json:"start-date"`
	EndDate   time.Time `json:"end-date"`
	Capacity  int       `json:"capacity"`
}

type StudioClassBooking struct {
	ID          int         `json:"id"`
	User        *User        `json:"user"`
	StudioClass *StudioClass `json:"studio-class"`
}

// StudioClassService represents a service for managing Studio Classes
type StudioClassService interface {
	// Creates a new studio class.
	CreateStudioClass(ctx context.Context, class *StudioClass) error

	// Makes a booking for a studio class booking for a user. Provided the user id and the class id
	BookStudioClass(ctx context.Context, userID int, classID int) (*StudioClassBooking, error)
}

// Validate returns an error if the studio class contains invalid fields.
// This only performs basic validation.
func (sc *StudioClass) Validate() error {
	if sc.Name == "" {
		return Errorf(EINVALID, "Studio class name required.")
	}

	if sc.Capacity == 0 {
		return Errorf(EINVALID, "Studio class can't have a capacity of 0.")
	}

	timeDiff := sc.EndDate.Sub(sc.StartDate)
	if string(timeDiff.String()[0]) == "-" {
		return Errorf(EINVALID, "Start Date can't be later than the End date")
	}

	return nil
}
