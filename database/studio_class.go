package database

import (
	"context"
	"fmt"
	"math/rand"

	gft "gitlab.com/mike18farad/glofox-technical-task.git"
)

// Ensure service implements interface.
var _ gft.StudioClassService = (*StudioClassService)(nil)

// StudioClassService represents a service for managing studio classes.
type StudioClassService struct {
	db *DB
}

// NewStudioClassService returns a new instance of StudioClassService.
func NewStudioClassService(db *DB) *StudioClassService {
	return &StudioClassService{db: db}
}

// CreateStudioClass creates a new studio class
func (s *StudioClassService) CreateStudioClass(ctx context.Context, class *gft.StudioClass) error {
	err := createStudioClass(ctx, s.db, class)
	if err != nil {
		return err
	}
	return nil
}

func (s *StudioClassService) BookStudioClass(ctx context.Context, studioClassID int, userID int) (*gft.StudioClassBooking, error) {

	studioClass, err := s.FindStudioClass(ctx, studioClassID)
	if err != nil {
		return nil, err
	}

	user, err := findUser(ctx, s.db, userID)
	if err != nil {
		return nil, err
	}

	classBooking := &gft.StudioClassBooking{
		ID:          rand.Intn(1000),
		User:        user,
		StudioClass: studioClass,
	}

	s.db.StudioClassBookings = append(s.db.StudioClassBookings, classBooking)

	return classBooking, nil
}

func (s *StudioClassService) FindStudioClass(ctx context.Context, studioClassID int) (*gft.StudioClass, error) {
	for _, class := range s.db.StudioClasses {
		if class.ID == studioClassID {
			return class, nil
		}
	}

	return nil, &gft.Error{Code: gft.ENOTFOUND, Message: "Studio class not found."}
}

func createStudioClass(ctx context.Context, db *DB, class *gft.StudioClass) error {
	// Perform basic field validation.
	if err := class.Validate(); err != nil {
		return fmt.Errorf("class of classID: %d is invalid; %w", class.ID, err)
	}

	db.StudioClasses = append(db.StudioClasses, class)
	return nil
}
