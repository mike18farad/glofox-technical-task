package database

import (
	"context"
	"reflect"
	"testing"

	gft "gitlab.com/mike18farad/glofox-technical-task.git"
)

const (
	TEST_USER_ID       = 30
	BAD_TEST_USER_ID   = 0
	TEST_USER_NAME     = "test-user"
	BAD_TEST_USER_NAME = ""

	TEST_STUDIO_CLASS_ID     = 40
	BAD_TEST_STUDIO_CLASS_ID = 0
	TEST_STUDIO_CLASS_NAME   = "test-class"
)

var (
	testStudioClass = &gft.StudioClass{
		ID:        TEST_STUDIO_CLASS_ID,
		Name:      TEST_STUDIO_CLASS_NAME,
		StartDate: getDateFromString("2020-01-29"),
		EndDate:   getDateFromString("2020-02-29"),
		Capacity:  10,
	}

	testUser = &gft.User{
		ID:   TEST_USER_ID,
		Name: TEST_USER_NAME,
		Roles: []*gft.UserRole{
			{
				Title: gft.NORMAL_USER_ROLE,
			},
		},
	}
)

func TestStudioClassService_CreateStudioClass(t *testing.T) {
	ctx := context.Background()

	testDB, err := NewDB()
	if err != nil {
		t.Errorf("Could not create a new DB for tests: %w", err)
	}

	type fields struct {
		db *DB
	}
	type args struct {
		ctx   context.Context
		class *gft.StudioClass
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Good Case: Studio class successfully created",
			fields: fields{
				db: testDB,
			},
			args: args{
				ctx: ctx,
				class: &gft.StudioClass{
					Name:      "Test Class",
					StartDate: getDateFromString("2020-01-29"),
					EndDate:   getDateFromString("2020-02-29"),
					Capacity:  10,
				},
			},
			wantErr: false,
		},
		{
			name: "Edge Case: Studio class not successfully created - empty name",
			fields: fields{
				db: testDB,
			},
			args: args{
				ctx: ctx,
				class: &gft.StudioClass{
					Name:      "",
					StartDate: getDateFromString("2020-01-29"),
					EndDate:   getDateFromString("2020-02-29"),
					Capacity:  10,
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StudioClassService{
				db: tt.fields.db,
			}
			if err := s.CreateStudioClass(tt.args.ctx, tt.args.class); (err != nil) != tt.wantErr {
				t.Errorf("StudioClassService.CreateStudioClass() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestStudioClassService_BookStudioClass(t *testing.T) {

	ctx := context.Background()

	testDB, err := NewDB()
	if err != nil {
		t.Errorf("Could not create a new DB for tests: %w", err)
	}

	err = createStudioClass(ctx, testDB, testStudioClass)
	if err != nil {
		t.Errorf("Could not create studio class for test; %w", err)
	}

	err = createUser(ctx, testDB, testUser)
	if err != nil {
		t.Errorf("Could not create user for test; %w", err)
	}

	type fields struct {
		db *DB
	}
	type args struct {
		ctx           context.Context
		studioClassID int
		userID        int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Good Case: Studio Booking successfully created",
			fields: fields{
				db: testDB,
			},
			args: args{
				ctx:           ctx,
				studioClassID: TEST_STUDIO_CLASS_ID,
				userID:        TEST_USER_ID,
			},
			wantErr: false,
		},
		{
			name: "Edge Case: Studio Booking not successfully created - studio class provided does not exist",
			fields: fields{
				db: testDB,
			},
			args: args{
				ctx:           ctx,
				studioClassID: BAD_TEST_STUDIO_CLASS_ID,
				userID:        TEST_USER_ID,
			},
			wantErr: true,
		},
		{
			name: "Edge Case: Studio Booking not successfully created - user provided does not exist",
			fields: fields{
				db: testDB,
			},
			args: args{
				ctx:           ctx,
				studioClassID: TEST_STUDIO_CLASS_ID,
				userID:        BAD_TEST_USER_ID,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StudioClassService{
				db: tt.fields.db,
			}
			got, err := s.BookStudioClass(tt.args.ctx, tt.args.studioClassID, tt.args.userID)
			if (err != nil) != tt.wantErr {
				t.Errorf("StudioClassService.BookStudioClass() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if (got == nil) != tt.wantErr {
				t.Errorf("nil StudioBooking returned, wantErr %v", tt.wantErr)
			}
		})
	}
}

func TestStudioClassService_FindStudioClass(t *testing.T) {
	ctx := context.Background()

	testDB, err := NewDB()
	if err != nil {
		t.Errorf("Could not create a new DB for tests: %w", err)
	}

	err = createStudioClass(ctx, testDB, testStudioClass)
	if err != nil {
		t.Errorf("Could not create studio class for test; %w", err)
	}

	type fields struct {
		db *DB
	}
	type args struct {
		ctx           context.Context
		studioClassID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *gft.StudioClass
		wantErr bool
	}{
		{
			name: "Good Case: Studio Class Found",
			fields: fields{
				db: testDB,
			},
			args: args{
				ctx:           ctx,
				studioClassID: TEST_STUDIO_CLASS_ID,
			},
			want:    testStudioClass,
			wantErr: false,
		},
		{
			name: "Edge Case: Studio Class Not Found",
			fields: fields{
				db: testDB,
			},
			args: args{
				ctx:           ctx,
				studioClassID: BAD_TEST_STUDIO_CLASS_ID,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StudioClassService{
				db: tt.fields.db,
			}
			got, err := s.FindStudioClass(tt.args.ctx, tt.args.studioClassID)
			if (err != nil) != tt.wantErr {
				t.Errorf("StudioClassService.FindStudioClass() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("StudioClassService.FindStudioClass() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_createStudioClass(t *testing.T) {
	ctx := context.Background()

	testDB, err := NewDB()
	if err != nil {
		t.Errorf("Could not create a new DB for tests: %w", err)
	}

	type args struct {
		ctx   context.Context
		db    *DB
		class *gft.StudioClass
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Good Case: Studio Class successfully created",
			args: args{
				ctx:   ctx,
				db:    testDB,
				class: testStudioClass,
			},
			wantErr: false,
		},
		{
			name: "Edge Case: Studio Class not successfully created - Invalid studio class provided",
			args: args{
				ctx:   ctx,
				db:    testDB,
				class: &gft.StudioClass{},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := createStudioClass(tt.args.ctx, tt.args.db, tt.args.class); (err != nil) != tt.wantErr {
				t.Errorf("createStudioClass() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
