package database

import (
	"context"
	"fmt"
	"time"

	gft "gitlab.com/mike18farad/glofox-technical-task.git"
)

const (
	DATE_FORMAT = "2006-01-02"
)

var (
	usersToPrepopulateDB = []*gft.User{
		{
			ID:   1,
			Name: "Jose Mourinho",
			Roles: []*gft.UserRole{
				{
					Title: gft.NORMAL_USER_ROLE,
				},
			},
		},
	}


	studioClassesToPrepopulateDB = []*gft.StudioClass{
		{
			ID:        1,
			Name:      "Pirates",
			StartDate: getDateFromString("2020-01-29"),
			EndDate:   getDateFromString("2020-02-29"),
			Capacity:  10,
		},
	}
)

type DB struct {
	StudioClasses       []*gft.StudioClass
	Users               []*gft.User
	StudioClassBookings []*gft.StudioClassBooking
}

func NewDB() (*DB, error) {

	db := &DB{}

	ctx := context.Background()
	_, err := generateStudioClasses(ctx, studioClassesToPrepopulateDB, db)
	if err != nil {
		return nil, fmt.Errorf("studio classes could not be generated: %w", err)
	}

	_, err = generateUsers(ctx, usersToPrepopulateDB, db)
	if err != nil {
		return nil, fmt.Errorf("users could not be generated: %w", err)
	}

	return db, nil

}

func getDateFromString(dateStr string) (time.Time) {
	date, err := time.Parse(DATE_FORMAT, dateStr)
	if err != nil {
		panic("Date Could Not Be parsed")
	}
	return date
}

// Populates the DB with the provided studio classes
func generateStudioClasses(ctx context.Context, studioClasses []*gft.StudioClass,  db *DB) ([]*gft.StudioClass, error)  {

	for _, class := range studioClasses {
		err := createStudioClass(ctx, db, class)
		if err != nil {
			return nil, err
		}
	}

	return db.StudioClasses, nil
}

// Populates the DB with the provided users
func generateUsers(ctx context.Context, users []*gft.User, db *DB) ([]*gft.User, error) {

	for _, user := range users {
		err := createUser(ctx, db, user)
		if err != nil {
			return nil, err
		}
	}

	return db.Users, nil
}
