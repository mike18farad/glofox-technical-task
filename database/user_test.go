package database

import (
	"context"
	"reflect"
	"testing"

	gft "gitlab.com/mike18farad/glofox-technical-task.git"
)

func TestUserService_CreateUser(t *testing.T) {

	ctx := context.Background()

	testDB, err := NewDB()
	if err != nil {
		t.Errorf("Could not create a new DB for tests: %w", err)
	}

	type fields struct {
		db *DB
	}
	type args struct {
		ctx  context.Context
		user *gft.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Good Case: User successfully created",
			fields: fields{
				db: testDB,
			},
			args: args{
				ctx: ctx,
				user: &gft.User{
					ID:   TEST_USER_ID,
					Name: TEST_USER_NAME,
					Roles: []*gft.UserRole{
						{
							Title: gft.NORMAL_USER_ROLE,
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "Edge Case: User not successfully created - Invalid User supplied (Empty username)",
			fields: fields{
				db: testDB,
			},
			args: args{
				ctx: ctx,
				user: &gft.User{
					ID:   TEST_USER_ID,
					Name: BAD_TEST_USER_NAME,
					Roles: []*gft.UserRole{
						{
							Title: gft.NORMAL_USER_ROLE,
						},
					},
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &UserService{
				db: tt.fields.db,
			}
			if err := s.CreateUser(tt.args.ctx, tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("UserService.CreateUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserService_FindUser(t *testing.T) {

	ctx := context.Background()

	testDB, err := NewDB()
	if err != nil {
		t.Errorf("Could not create a new DB for tests: %w", err)
	}

	err = createUser(ctx, testDB, testUser)
	if err != nil {
		t.Errorf("Could not create user for test; %w", err)
	}

	type fields struct {
		db *DB
	}
	type args struct {
		ctx    context.Context
		userID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *gft.User
		wantErr bool
	}{
		{
			name: "Good Case: User successfully retreived",
			fields: fields{
				db: testDB,
			},
			args: args{
				ctx:    ctx,
				userID: TEST_USER_ID,
			},
			want:    testUser,
			wantErr: false,
		},
		{
			name: "Edge Case: User not successfully retreived - user with user ID not present",
			fields: fields{
				db: testDB,
			},
			args: args{
				ctx:    ctx,
				userID: BAD_TEST_USER_ID,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &UserService{
				db: tt.fields.db,
			}
			got, err := s.FindUser(tt.args.ctx, tt.args.userID)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserService.FindUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserService.FindUser() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_findUser(t *testing.T) {

	ctx := context.Background()

	testDB, err := NewDB()
	if err != nil {
		t.Errorf("Could not create a new DB for tests: %w", err)
	}

	err = createUser(ctx, testDB, testUser)
	if err != nil {
		t.Errorf("Could not create user for test; %w", err)
	}

	type args struct {
		ctx    context.Context
		db     *DB
		userID int
	}
	tests := []struct {
		name    string
		args    args
		want    *gft.User
		wantErr bool
	}{
		{
			name: "Good Case: User successfully retrieved",
			args: args{
				ctx:    ctx,
				db:     testDB,
				userID: TEST_USER_ID,
			},
			want:    testUser,
			wantErr: false,
		},
		{
			name: "Edge Case: User not successfully retrieved - user with user ID not present",
			args: args{
				ctx:    ctx,
				db:     testDB,
				userID: BAD_TEST_USER_ID,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := findUser(tt.args.ctx, tt.args.db, tt.args.userID)
			if (err != nil) != tt.wantErr {
				t.Errorf("findUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("findUser() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_createUser(t *testing.T) {

	ctx := context.Background()

	testDB, err := NewDB()
	if err != nil {
		t.Errorf("Could not create a new DB for tests: %w", err)
	}

	type args struct {
		ctx  context.Context
		db   *DB
		user *gft.User
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Good Case: user successfully created",
			args: args{
				ctx: ctx,
				db: testDB,
				user: testUser,
			},
			wantErr: false,
		},
		{
			name: "Edge Case: user not successfully created - invalid user provided",
			args: args{
				ctx: ctx,
				db: testDB,
				user: &gft.User{},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := createUser(tt.args.ctx, tt.args.db, tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("createUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
