package database

import (
	"context"
	"testing"

	gft "gitlab.com/mike18farad/glofox-technical-task.git"
)

func Test_generateStudioClasses(t *testing.T) {
	ctx := context.Background()

	testDB, err := NewDB()
	if err != nil {
		t.Errorf("Could not create a new DB for tests: %w", err)
	}

	type args struct {
		ctx           context.Context
		studioClasses []*gft.StudioClass
		db            *DB
	}
	tests := []struct {
		name    string
		args    args
		want    []*gft.StudioClass
		wantErr bool
	}{
		{
			name: "Good Case: successfully generated studio classes",
			args: args{
				ctx: ctx,
				studioClasses: []*gft.StudioClass{testStudioClass},
				db: testDB,
			},
			wantErr: false,
		},
		{
			name: "Edge Case: studio classes not successfully generated",
			args: args{
				ctx: ctx,
				studioClasses: []*gft.StudioClass{{}},
				db: testDB,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := generateStudioClasses(tt.args.ctx, tt.args.studioClasses, tt.args.db)
			if (err != nil) != tt.wantErr {
				t.Errorf("generateStudioClasses() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if (got == nil) != tt.wantErr {
				t.Errorf("nil StudioClasses returned, wantErr %v", tt.wantErr)
			}
		})
	}
}

func Test_generateUsers(t *testing.T) {
	ctx := context.Background()

	testDB, err := NewDB()
	if err != nil {
		t.Errorf("Could not create a new DB for tests: %w", err)
	}

	type args struct {
		ctx   context.Context
		users []*gft.User
		db    *DB
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Good Case: successfully generated users",
			args: args{
				ctx: ctx,
				users: []*gft.User{testUser},
				db: testDB,
			},
			wantErr: false,
		},
		{
			name: "Edge Case: studio classes not successfully generated",
			args: args{
				ctx: ctx,
				users: []*gft.User{{}},
				db: testDB,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := generateUsers(tt.args.ctx, tt.args.users, tt.args.db)
			if (err != nil) != tt.wantErr {
				t.Errorf("generateUsers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if (got == nil) != tt.wantErr {
				t.Errorf("nil Users returned, wantErr %v", tt.wantErr)
			}
		})
	}
}
