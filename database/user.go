package database

import (
	"context"
	"fmt"

	gft "gitlab.com/mike18farad/glofox-technical-task.git"
)

// Ensure service implements interface.
var _ gft.UserService = (*UserService)(nil)

// UserService represents a service for managing users.
type UserService struct {
	db *DB
}

// NewUserService returns a new instance of UserService.
func NewUserService(db *DB) *UserService {
	return &UserService{db: db}
}

// Creates a new User
func (s *UserService) CreateUser(ctx context.Context, user *gft.User) error {
	err := createUser(ctx, s.db, user)
	if err != nil {
		return err
	}
	return nil
}

// Searches and returns user with the provided userID
func (s *UserService) FindUser(ctx context.Context, userID int) (*gft.User, error) {
	user, err := findUser(ctx, s.db, userID)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func findUser(ctx context.Context, db *DB, userID int) (*gft.User, error) {
	for _, user := range db.Users {
		if user.ID == userID {
			return user, nil
		}
	}

	return nil, &gft.Error{Code: gft.ENOTFOUND, Message: "user not found."}
}

func createUser(ctx context.Context, db *DB, user *gft.User) error {
	// Perform basic field validation.
	if err := user.Validate(); err != nil {
		return fmt.Errorf("userID: %d is invalid; %w", user.ID, err)
	}

	db.Users = append(db.Users, user)
	return nil
}
