package glofoxtechnicaltask_test

import (
	"testing"
	"time"

	gft "gitlab.com/mike18farad/glofox-technical-task.git"
)

func TestStudioClass_Validate(t *testing.T) {
	type fields struct {
		ID        int
		Name      string
		StartDate time.Time
		EndDate   time.Time
		Capacity  int
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "Good Case: Valid Studio Class data fields provided",
			fields: fields{
				ID:        1,
				Name:      "test-studio-class",
				StartDate: time.Now(),
				EndDate:   time.Now().Add(time.Hour * 480),
				Capacity:  10,
			},
			wantErr: false,
		},
		{
			name: "Edge Case: Invalid Studio Class data fields provided - Empty class name",
			fields: fields{
				ID:        1,
				Name:      "",
				StartDate: time.Now(),
				EndDate:   time.Now().Add(time.Hour * 480),
				Capacity:  10,
			},
			wantErr: true,
		},
		{
			name: "Edge Case: Invalid Studio Class data fields provided - Zero capacity",
			fields: fields{
				ID:        1,
				Name:      "test-studio-class",
				StartDate: time.Now(),
				EndDate:   time.Now().Add(time.Hour * 480),
				Capacity:  0,
			},
			wantErr: true,
		},
		{
			name: "Edge Case: Invalid Studio Class data fields provided - Invalid start & end date",
			fields: fields{
				ID:        1,
				Name:      "test-studio-class",
				StartDate: time.Now().Add(time.Hour * 480),
				EndDate:   time.Now(),
				Capacity:  10,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sc := &gft.StudioClass{
				ID:        tt.fields.ID,
				Name:      tt.fields.Name,
				StartDate: tt.fields.StartDate,
				EndDate:   tt.fields.EndDate,
				Capacity:  tt.fields.Capacity,
			}
			if err := sc.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("StudioClass.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
