# Glofox Technical Task

[![pipeline status](https://gitlab.com/mike18farad/glofox-technical-task/badges/develop/pipeline.svg)](https://gitlab.com/mike18farad/glofox-technical-task/-/commits/develop) [![coverage report](https://gitlab.com/mike18farad/glofox-technical-task/badges/develop/coverage.svg)](https://gitlab.com/mike18farad/glofox-technical-task/-/commits/develop)

Glofox technical task assessment

### AppName Abbreviation

gft -> Glofox Technical Assessment (Couldn't Think of a better App name Lol)

## Usage

```bash
go run cmd/gftd/main.go
```

A url with a URL with a random port will be generated and printed in your console

#### Example:

```bash
2021/07/12 17:48:03 running: url="http://localhost:37931"
```

### Note:
I chose to use an In memory database that is prepopulated with the following values on App Initialisation

```json
// studio classes
[
  {
    "id": 1,
    "name": "Pirates",
    "start-date": "2020-01-29T00:00:00Z",
    "end-date": "2020-02-29T00:00:00Z",
    "capacity": 10
  }
]

// users
[
 {
    "id": 1,
    "name": "Jose Mourinho",
    "roles": [{ "title": "NORMAL" }]
 }
]
```

### Create Class

```bash
curl -v -i -H "Accept: application/json" -H "Content-Type: application/json" -d '{
    "id": 4,
    "name": "",
    "start-date": "2021-02-18T21:54:42.123Z",
    "end-date": "2021-03-18T21:54:42.123Z",
    "capacity": 10
}' http://localhost:37931/classes
```

Expected Response: A json payload of the created class with a `201` http status response

```json
{
  "id": 4,
  "name": "bro-class",
  "start-date": "2021-02-18T21:54:42.123Z",
  "end-date": "2021-03-18T21:54:42.123Z",
  "capacity": 10
}
```

### Create Class Booking

```bash
curl -v -i -H "Accept: application/json" -H "Content-Type: application/json" -d '{
    "user-id": 1,
    "class-id": 1
}' http://localhost:37931/bookings
```

Expected Response: A json payload of the created class booking with a `201` http status response

```json
{
  "id": 81,
  "user": {
    "id": 1,
    "name": "Jose Mourinho",
    "roles": [{ "title": "NORMAL" }]
  },
  "studio-class": {
    "id": 1,
    "name": "Pirates",
    "start-date": "2020-01-29T00:00:00Z",
    "end-date": "2020-02-29T00:00:00Z",
    "capacity": 10
  }
}
```
