package glofoxtechnicaltask

import "context"

const (
	NORMAL_USER_ROLE = "NORMAL"
)

// User represents a user in the system.
type User struct {
	ID    int         `json:"id"`
	Name  string      `json:"name"`
	Roles []*UserRole `json:"roles"`
}

// UserRole represents the roles that a user may have in the system
type UserRole struct {
	Title string `json:"title"`
}

// UserService represents a service for managing Users
type UserService interface {
	// Creates a new user.
	CreateUser(ctx context.Context, class *User) error
	// Searches and returns user with the provided ID
	FindUser(ctx context.Context, userID int) (*User, error)
}

// Validate returns an error if the user contains invalid fields.
// This only performs basic validation.
func (u *User) Validate() error {
	if u.Name == "" {
		return Errorf(EINVALID, "User name required.")
	}
	return nil
}
