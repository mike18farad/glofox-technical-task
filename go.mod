module gitlab.com/mike18farad/glofox-technical-task.git

go 1.15

require (
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
