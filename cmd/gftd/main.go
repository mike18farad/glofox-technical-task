package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"

	gft "gitlab.com/mike18farad/glofox-technical-task.git"
	database "gitlab.com/mike18farad/glofox-technical-task.git/database"
	"gitlab.com/mike18farad/glofox-technical-task.git/http"
)

const (
	HTTPAddress = "HTTP_ADDRESS"
	Domain      = "DOMAIN"
)

func main() {
	// Setup signal handlers.
	ctx, cancel := context.WithCancel(context.Background())
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() { <-c; cancel() }()

	// Instantiate a new type to represent our application.
	// This type lets us shared setup code with our end-to-end tests.
	m, err := NewMain(ctx)
	if err != nil {
		log.Panicf("new main could not be created: %v", err)
		os.Exit(1)
	}

	// Execute program.
	if err := m.Run(ctx); err != nil {
		m.Close()
		fmt.Fprintln(os.Stderr, err)
		gft.ReportError(ctx, err)
		os.Exit(1)
	}

	// Wait for CTRL-C.
	<-ctx.Done()

	// Clean up program.
	if err := m.Close(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

}

// Main represents the program.
type Main struct {
	// HTTP server for handling HTTP communication.
	HTTPServer *http.Server

	Database *database.DB
}

// NewMain returns a new instance of Main.
func NewMain(ctx context.Context) (*Main, error) {

	db, err := database.NewDB()
	if err != nil {
		return nil, err
	}

	return &Main{
		Database:   db,
		HTTPServer: http.NewServer(),
	}, nil
}

// Close gracefully stops the program.
func (m *Main) Close() error {
	if m.HTTPServer != nil {
		if err := m.HTTPServer.Close(); err != nil {
			return err
		}
	}

	return nil
}

// Run executes the program. The configuration should already be set up before
// calling this function.
func (m *Main) Run(ctx context.Context) (err error) {
	// Initialize error tracking.

	// Instantiate Domain services.
	userService := database.NewUserService(m.Database)
	studioClassService := database.NewStudioClassService(m.Database)

	// Copy configuration settings to the HTTP server.
	httpAddress := os.Getenv(HTTPAddress)
	domain := os.Getenv(Domain)

	m.HTTPServer.Addr = httpAddress
	m.HTTPServer.Domain = domain
	m.HTTPServer.UserService = userService
	m.HTTPServer.StudioClassService = studioClassService

	// Start the HTTP server.
	if err := m.HTTPServer.Open(); err != nil {
		return err
	}

	// If TLS enabled, redirect non-TLS connections to TLS.
	if m.HTTPServer.UseTLS() {
		go func() {
			log.Fatal(http.ListenAndServeTLSRedirect(domain))
		}()
	}

	log.Printf("running: url=%q", m.HTTPServer.URL())

	return nil
}
