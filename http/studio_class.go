package http

import (
	"encoding/json"
	"net/http"

	gft "gitlab.com/mike18farad/glofox-technical-task.git"
)

type StudioClassBookingRequest struct {
	UserID  int `json:"user-id"`
	ClassID int `json:"class-id"`
}

// handleStudioClassCreate handles the "POST /classes"
// It reads & writes data using with JSON.
func (s *Server) handleStudioClassCreate(w http.ResponseWriter, r *http.Request) {
	// Unmarshal data based on HTTP request's content type.
	var studioClass gft.StudioClass
	switch r.Header.Get("Content-type") {
	case "application/json":
		if err := json.NewDecoder(r.Body).Decode(&studioClass); err != nil {
			Error(w, r, gft.Errorf(gft.EINVALID, "Invalid JSON body"))
			return
		}
	}

	// Create studioClass in the database.
	err := s.StudioClassService.CreateStudioClass(r.Context(), &studioClass)

	// Write new studioClass content to response based on accept header.
	switch r.Header.Get("Accept") {
	case "application/json":
		if err != nil {
			Error(w, r, err)
			return
		}

		w.Header().Set("Content-type", "application/json")
		w.WriteHeader(http.StatusCreated)
		if err := json.NewEncoder(w).Encode(studioClass); err != nil {
			LogError(r, err)
			return
		}

	}
}

// handleStudioClassBooking handles the "POST /bookings"
// It reads & writes data using with JSON.
func (s *Server) handleStudioClassBooking(w http.ResponseWriter, r *http.Request) {
	// Unmarshal data based on HTTP request's content type.
	var bookRequest StudioClassBookingRequest
	switch r.Header.Get("Content-type") {
	case "application/json":
		if err := json.NewDecoder(r.Body).Decode(&bookRequest); err != nil {
			Error(w, r, gft.Errorf(gft.EINVALID, "Invalid JSON body"))
			return
		}
	}

	// Create studioClass in the database.
	booking, err := s.StudioClassService.BookStudioClass(r.Context(), bookRequest.UserID, bookRequest.ClassID)

	// Write new studioClass content to response based on accept header.
	switch r.Header.Get("Accept") {
	case "application/json":
		if err != nil {
			Error(w, r, err)
			return
		}

		w.Header().Set("Content-type", "application/json")
		w.WriteHeader(http.StatusCreated)
		if err := json.NewEncoder(w).Encode(booking); err != nil {
			LogError(r, err)
			return
		}

	}
}
