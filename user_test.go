package glofoxtechnicaltask_test

import (
	"testing"

	gft "gitlab.com/mike18farad/glofox-technical-task.git"
)

func TestUser_Validate(t *testing.T) {
	type fields struct {
		ID    int
		Name  string
		Roles []*gft.UserRole
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "Good Case: Valid user details provided",
			fields: fields{
				ID:   1,
				Name: "test-user",
				Roles: []*gft.UserRole{
					{
						Title: gft.NORMAL_USER_ROLE,
					},
				},
			},
			wantErr: false,
		},
		{
			name: "Edge Case: Invalid user details provided - empty name",
			fields: fields{
				ID:   1,
				Name: "",
				Roles: []*gft.UserRole{
					{
						Title: gft.NORMAL_USER_ROLE,
					},
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &gft.User{
				ID:    tt.fields.ID,
				Name:  tt.fields.Name,
				Roles: tt.fields.Roles,
			}
			if err := u.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("User.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
